const s1 = Symbol()
const s2 = Symbol()
// console.log(s1)
// console.log(s1 === s2)   //false
const s3 = Symbol('dy')
// console.log(s3)
const s4 = Symbol('dy')
// console.log(s3 === s4)   //false
// console.log(s4.toString())  //Symbol(dy)
// console.log( Boolean(s4) )  //true
// console.log( !s4 )          //false
// const s5 = Symbol( { a: 111 } )     //Symbol([object Object])

let prop = 'name'

const obj = {
    [prop]: 'name'
}
// console.log(obj)

const s5 = Symbol('name')
let obj1 = {
    [s5]: 'lison',
    a:1
}
// console.log(obj1)
// console.log(obj1[s5])

for(const key in obj1) {
    console.log(key)
}

// console.log( Object.keys(obj1) )
// console.log( Object.getOwnPropertyNames(obj1) )
// console.log( JSON.stringify(obj1) )
// console.log( Object.getOwnPropertySymbols(obj1) )
// console.log( Reflect.ownKeys(obj1) )

// Symbol.keyFor() Symbol.for()
const s6 = Symbol.for( 'haha' )
const s7 = Symbol.for( 'haha' )
// console.log(s6 === s7);        //true

// console.log( Symbol.keyFor(s6) )