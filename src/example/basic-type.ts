let bool:boolean
bool = true
// console.log(bool)


// 枚举类型
enum Roles {
    SUPER_ADMIN,
    ADMIN = 4,
    USER
}
// console.log(Roles.ADMIN)   //4
// console.log(Roles[4])      //ADMIN

var getObj = function(obj: object): void{
    console.log( obj )
}

// getObj( { a:1 } )


// 类型断言
const getLength = ( target: string | number ):number => {
    if( (<string>target).length || ( target as string ).length == 0 ){
        return (<string>target).length
    }else{
        return target.toString().length
    }
}
// console.log(getLength(123))
