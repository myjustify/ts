const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
config = {
    ...require('./common.js'),
    mode: 'development',
    devServer: {
        compress: true,
        host: 'localhost',
        port: '9527',
        open: true,
        contentBase: path.join(__dirname, '../dist')
    },
    devtool: 'inline-source-map',
    plugins: [
        new HtmlWebpackPlugin({
            title: 'dev-webpack',
            template: path.resolve(__dirname,'../src/index.html')
        }),
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: ['dist'],
        })
    ]
}
module.exports = config