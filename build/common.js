const path = require('path');
module.exports = {
    mode: 'production',
    entry: path.resolve(__dirname,'../src/index.ts'),
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, '../dist')
    },
    resolve:{
        extensions: ['.js','jsx','.ts', '.tsx' ]
    },
    module: {
        rules: [{
            test: /\.tsx?$/,
            use: 'ts-loader',
            exclude: /node_modules/
        }]
    },
}