const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
module.exports = {
    ...require('./common.js'),
    plugins: [
        new HtmlWebpackPlugin({
            title: 'build-webpack',
            template: path.resolve(__dirname,'../src/index.html')
        }),
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: ['dist'],
        })
    ]
}